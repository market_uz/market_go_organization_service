CREATE TABLE IF NOT EXISTS "branches" (
    id UUID PRIMARY KEY,
    branch_code VARCHAR NOT NULL,
    name VARCHAR(50) NOT NULL,
    address VARCHAR(50) NOT NULL,
    phone VARCHAR(13) NOT NULL,
    shop_id UUID REFERENCES shops(id),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP
);