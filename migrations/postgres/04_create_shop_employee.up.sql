CREATE TABLE IF NOT EXISTS "shop_employees" (
    "id" UUID PRIMARY KEY,
    "shop_id" UUID REFERENCES shops(id),
    "employee_id" UUID REFERENCES employees(id),
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);

CREATE UNIQUE INDEX idx_shop_employee ON shop_employees (shop_id, employee_id);