CREATE TABLE IF NOT EXISTS "couriers" (
    "id" UUID PRIMARY KEY,
    "courier_name" VARCHAR(50) NOT NULL,
    "courier_phone" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);