CREATE TABLE IF NOT EXISTS "employees" (
    "id" UUID PRIMARY KEY,
    "employee_name" VARCHAR(50) NOT NULL,
    "employee_phone" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);