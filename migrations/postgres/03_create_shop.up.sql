CREATE TABLE IF NOT EXISTS "shops" (
    "id" UUID PRIMARY KEY,
    "shop_name" VARCHAR(50) NOT NULL,
    "created_at" TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP 
);