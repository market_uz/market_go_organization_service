package service

import (
	"app/config"
	"app/genproto/organization_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type EmployeeService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedEmployeeServiceServer
}

func NewEmployeeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *EmployeeService {
	return &EmployeeService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *EmployeeService) Create(ctx context.Context, req *organization_service.CreateEmployee) (resp *organization_service.Employee, err error) {

	i.log.Info("---CreateEmployee------>", logger.Any("req", req))

	pKey, err := i.strg.Employee().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateEmployee->Employee->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Employee().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyEmployee->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EmployeeService) GetByID(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *organization_service.Employee, err error) {

	i.log.Info("---GetEmployeeByID------>", logger.Any("req", req))

	resp, err = i.strg.Employee().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetEmployeeByID->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EmployeeService) GetList(ctx context.Context, req *organization_service.GetListEmployeeRequest) (resp *organization_service.GetListEmployeeResponse, err error) {

	i.log.Info("---GetEmployees------>", logger.Any("req", req))

	resp, err = i.strg.Employee().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetEmployees->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *EmployeeService) Update(ctx context.Context, req *organization_service.UpdateEmployee) (resp *organization_service.Employee, err error) {

	i.log.Info("---UpdateEmployee------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Employee().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateEmployee--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Employee().GetByPKey(ctx, &organization_service.EmployeePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetEmployee->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *EmployeeService) UpdatePatch(ctx context.Context, req *organization_service.UpdatePatchEmployee) (resp *organization_service.Employee, err error) {

	i.log.Info("---UpdatePatchEmployee------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Employee().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchEmployee--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Employee().GetByPKey(ctx, &organization_service.EmployeePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetEmployee->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *EmployeeService) Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteEmployee------>", logger.Any("req", req))

	err = i.strg.Employee().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteEmployee->Employee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
