package service

import (
	"app/config"
	"app/genproto/organization_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ShopService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedShopServiceServer
}

func NewShopService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ShopService {
	return &ShopService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ShopService) Create(ctx context.Context, req *organization_service.CreateShop) (resp *organization_service.Shop, err error) {

	i.log.Info("---CreateShop------>", logger.Any("req", req))

	pKey, err := i.strg.Shop().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateShop->Shop->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Shop().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyShop->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopService) GetByID(ctx context.Context, req *organization_service.ShopPrimaryKey) (resp *organization_service.Shop, err error) {

	i.log.Info("---GetShopByID------>", logger.Any("req", req))

	resp, err = i.strg.Shop().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShopByID->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopService) GetList(ctx context.Context, req *organization_service.GetListShopRequest) (resp *organization_service.GetListShopResponse, err error) {

	i.log.Info("---GetShops------>", logger.Any("req", req))

	resp, err = i.strg.Shop().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShops->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopService) Update(ctx context.Context, req *organization_service.UpdateShop) (resp *organization_service.Shop, err error) {

	i.log.Info("---UpdateShop------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Shop().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShop--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shop().GetByPKey(ctx, &organization_service.ShopPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShop->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShopService) UpdatePatch(ctx context.Context, req *organization_service.UpdatePatchShop) (resp *organization_service.Shop, err error) {

	i.log.Info("---UpdatePatchShop------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Shop().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchShop--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shop().GetByPKey(ctx, &organization_service.ShopPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShop->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShopService) Delete(ctx context.Context, req *organization_service.ShopPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteShop------>", logger.Any("req", req))

	err = i.strg.Shop().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteShop->Shop->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
