package service

import (
	"app/config"
	"app/genproto/organization_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CourierService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedCourierServiceServer
}

func NewCourierService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *CourierService {
	return &CourierService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *CourierService) Create(ctx context.Context, req *organization_service.CreateCourier) (resp *organization_service.Courier, err error) {

	i.log.Info("---CreateCourier------>", logger.Any("req", req))

	pKey, err := i.strg.Courier().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateCourier->Courier->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.Courier().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyCourier->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CourierService) GetByID(ctx context.Context, req *organization_service.CourierPrimaryKey) (resp *organization_service.Courier, err error) {

	i.log.Info("---GetCourierByID------>", logger.Any("req", req))

	resp, err = i.strg.Courier().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCourierByID->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CourierService) GetList(ctx context.Context, req *organization_service.GetListCourierRequest) (resp *organization_service.GetListCourierResponse, err error) {

	i.log.Info("---GetCouriers------>", logger.Any("req", req))

	resp, err = i.strg.Courier().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetCouriers->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *CourierService) Update(ctx context.Context, req *organization_service.UpdateCourier) (resp *organization_service.Courier, err error) {

	i.log.Info("---UpdateCourier------>", logger.Any("req", req))

	rowsAffected, err := i.strg.Courier().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateCourier--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Courier().GetByPKey(ctx, &organization_service.CourierPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCourier->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CourierService) UpdatePatch(ctx context.Context, req *organization_service.UpdatePatchCourier) (resp *organization_service.Courier, err error) {

	i.log.Info("---UpdatePatchCourier------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Courier().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchCourier--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Courier().GetByPKey(ctx, &organization_service.CourierPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetCourier->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *CourierService) Delete(ctx context.Context, req *organization_service.CourierPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteCourier------>", logger.Any("req", req))

	err = i.strg.Courier().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteCourier->Courier->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
