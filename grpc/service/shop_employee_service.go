package service

import (
	"app/config"
	"app/genproto/organization_service"
	"app/grpc/client"
	"app/models"
	"app/pkg/logger"
	"app/storage"
	"context"

	"github.com/golang/protobuf/ptypes/empty"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type ShopEmployeeService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*organization_service.UnimplementedShopEmployeeServiceServer
}

func NewShopEmployeeService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *ShopEmployeeService {
	return &ShopEmployeeService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *ShopEmployeeService) Create(ctx context.Context, req *organization_service.CreateShopEmployee) (resp *organization_service.ShopEmployee, err error) {

	i.log.Info("---CreateShopEmployee------>", logger.Any("req", req))

	pKey, err := i.strg.ShopEmployee().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateShopEmployee->ShopEmployee->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.ShopEmployee().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyShopEmployee->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopEmployeeService) GetByID(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) (resp *organization_service.ShopEmployee, err error) {

	i.log.Info("---GetShopEmployeeByID------>", logger.Any("req", req))

	resp, err = i.strg.ShopEmployee().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShopEmployeeByID->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopEmployeeService) GetList(ctx context.Context, req *organization_service.GetListShopEmployeeRequest) (resp *organization_service.GetListShopEmployeeResponse, err error) {

	i.log.Info("---GetShopEmployees------>", logger.Any("req", req))

	resp, err = i.strg.ShopEmployee().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetShopEmployees->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *ShopEmployeeService) Update(ctx context.Context, req *organization_service.UpdateShopEmployee) (resp *organization_service.ShopEmployee, err error) {

	i.log.Info("---UpdateShopEmployee------>", logger.Any("req", req))

	rowsAffected, err := i.strg.ShopEmployee().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateShopEmployee--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ShopEmployee().GetByPKey(ctx, &organization_service.ShopEmployeePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShopEmployee->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShopEmployeeService) UpdatePatch(ctx context.Context, req *organization_service.UpdatePatchShopEmployee) (resp *organization_service.ShopEmployee, err error) {

	i.log.Info("---UpdatePatchShopEmployee------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.ShopEmployee().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchShopEmployee--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.ShopEmployee().GetByPKey(ctx, &organization_service.ShopEmployeePrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetShopEmployee->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *ShopEmployeeService) Delete(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteShopEmployee------>", logger.Any("req", req))

	err = i.strg.ShopEmployee().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteShopEmployee->ShopEmployee->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}
