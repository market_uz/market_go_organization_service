package grpc

import (
	"app/config"
	"app/genproto/organization_service"
	"app/grpc/client"
	"app/grpc/service"
	"app/pkg/logger"
	"app/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	organization_service.RegisterCourierServiceServer(grpcServer, service.NewCourierService(cfg, log, strg, srvc))
	organization_service.RegisterEmployeeServiceServer(grpcServer, service.NewEmployeeService(cfg, log, strg, srvc))
	organization_service.RegisterShopServiceServer(grpcServer, service.NewShopService(cfg, log, strg, srvc))
	organization_service.RegisterShopEmployeeServiceServer(grpcServer, service.NewShopEmployeeService(cfg, log, strg, srvc))
	organization_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
