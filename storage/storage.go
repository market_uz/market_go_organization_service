package storage

import (
	"app/genproto/organization_service"
	"app/models"
	"context"
)

type StorageI interface {
	CloseDB()
	Courier() CourierRepoI
	Employee() EmployeeRepoI
	Shop() ShopRepoI
	ShopEmployee() ShopEmployeeRepoI
	Branch() BranchRepoI 
}

type CourierRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateCourier) (resp *organization_service.CourierPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.CourierPrimaryKey) (resp *organization_service.Courier, err error)
	GetAll(ctx context.Context, req *organization_service.GetListCourierRequest) (resp *organization_service.GetListCourierResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateCourier) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.CourierPrimaryKey) error
}

type EmployeeRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateEmployee) (resp *organization_service.EmployeePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *organization_service.Employee, err error)
	GetAll(ctx context.Context, req *organization_service.GetListEmployeeRequest) (resp *organization_service.GetListEmployeeResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateEmployee) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) error
}

type ShopRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateShop) (resp *organization_service.ShopPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.ShopPrimaryKey) (resp *organization_service.Shop, err error)
	GetAll(ctx context.Context, req *organization_service.GetListShopRequest) (resp *organization_service.GetListShopResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateShop) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.ShopPrimaryKey) error
}

type ShopEmployeeRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateShopEmployee) (resp *organization_service.ShopEmployeePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) (resp *organization_service.ShopEmployee, err error)
	GetAll(ctx context.Context, req *organization_service.GetListShopEmployeeRequest) (resp *organization_service.GetListShopEmployeeResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateShopEmployee) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *organization_service.CreateBranch) (resp *organization_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *organization_service.BranchPrimaryKey) (resp *organization_service.Branch, err error)
	GetAll(ctx context.Context, req *organization_service.GetListBranchRequest) (resp *organization_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *organization_service.UpdateBranch) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error
}