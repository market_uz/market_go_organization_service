package postgres

import (
	"app/genproto/organization_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type courierRepo struct {
	db *pgxpool.Pool
}

func NewCourierRepo(db *pgxpool.Pool) *courierRepo {
	return &courierRepo{
		db: db,
	}
}

func (c *courierRepo) Create(ctx context.Context, req *organization_service.CreateCourier) (resp *organization_service.CourierPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "couriers" (
			id, 
			courier_name,
			courier_phone,
			updated_at
		) VALUES ($1, $2, $3, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.CourierName,
		req.CourierPhone,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.CourierPrimaryKey{Id: id}, nil
}

func (c *courierRepo) GetByPKey(ctx context.Context, req *organization_service.CourierPrimaryKey) (resp *organization_service.Courier, err error) {
	query := `
		SELECT
			id,
			courier_name,
			courier_phone,
			created_at,
			updated_at
		FROM "couriers"
		WHERE id = $1
	`

	var (
		id            sql.NullString
		courier_name  sql.NullString
		courier_phone sql.NullString
		created_at    sql.NullString
		updated_at    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&courier_name,
		&courier_phone,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &organization_service.Courier{
		Id:           id.String,
		CourierName:  courier_name.String,
		CourierPhone: courier_phone.String,
		CreatedAt:    created_at.String,
		UpdatedAt:    updated_at.String,
	}

	return
}

func (c *courierRepo) GetAll(ctx context.Context, req *organization_service.GetListCourierRequest) (resp *organization_service.GetListCourierResponse, err error) {
	resp = &organization_service.GetListCourierResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			courier_name,
			courier_phone,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "couriers"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id            sql.NullString
			courier_name  sql.NullString
			courier_phone sql.NullString
			created_at    sql.NullString
			updated_at    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&courier_name,
			&courier_phone,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Couriers = append(resp.Couriers, &organization_service.Courier{
			Id:           id.String,
			CourierName:  courier_name.String,
			CourierPhone: courier_phone.String,
			CreatedAt:    created_at.String,
			UpdatedAt:    updated_at.String,
		})
	}

	return
}

func (c *courierRepo) Update(ctx context.Context, req *organization_service.UpdateCourier) (rowsAffected int64, err error) {
	var (
		query string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"couriers"
		SET
			courier_name = :courier_name,
			courier_phone = :courier_phone,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id": req.GetId(),
		"courier_name": req.GetCourierName(),
		"courier_phone": req.GetCourierPhone(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *courierRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set = " SET "
		ind = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"couriers"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *courierRepo) Delete(ctx context.Context, req *organization_service.CourierPrimaryKey) error {
	query := `DELETE FROM "couriers" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}