package postgres

import (
	"app/genproto/organization_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type branchRepo struct {
	db *pgxpool.Pool
}

func NewbranchRepo(db *pgxpool.Pool) *branchRepo {
	return &branchRepo{
		db: db,
	}
}

func (c *branchRepo) Create(ctx context.Context, req *organization_service.CreateBranch) (resp *organization_service.BranchPrimaryKey, err error) {
	id := uuid.New().String()

	query := `
		INSERT INTO "branches" (
			id,
			branch_code, 
			name,
			address,
			phone,
			shop_id,
			updated_at
		) VALUES ($1, $2, $3, $4, $5, $6, NOW())
	`

	branchCode := helper.GenerateBranchCode()

	_, err = c.db.Exec(ctx,
		query,
		id,
		branchCode,
		req.BranchName,
		req.Address,
		req.Phone,
		req.ShopId,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.BranchPrimaryKey{Id: id}, nil
}

func (c *branchRepo) GetByPKey(ctx context.Context, req *organization_service.BranchPrimaryKey) (resp *organization_service.Branch, err error) {
	query := `
		SELECT
			id,
			branch_code,
			name,
			address,
			phone,
			shop_id,
			created_at,
			updated_at
		FROM "branches"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		branch_code sql.NullString
		name        sql.NullString
		address     sql.NullString
		phone       sql.NullString
		shop_id     sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_code,
		&name,
		&address,
		&phone,
		&shop_id,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &organization_service.Branch{
		Id:         id.String,
		BranchCode: branch_code.String,
		BranchName: name.String,
		Address:    address.String,
		Phone:      phone.String,
		ShopId:     shop_id.String,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}

	return
}

func (c *branchRepo) GetAll(ctx context.Context, req *organization_service.GetListBranchRequest) (resp *organization_service.GetListBranchResponse, err error) {
	resp = &organization_service.GetListBranchResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			branch_code,
			name,
			address,
			phone,
			shop_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "branches"
	`

	if len(req.GetSearch()) > 0 {
		filter += " AND name ILIKE '%' || '" + req.Search + "' || '%' "
	}
	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id          sql.NullString
			branch_code sql.NullString
			name        sql.NullString
			address     sql.NullString
			phone       sql.NullString
			shop_id     sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_code,
			&name,
			&address,
			&phone,
			&shop_id,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Branches = append(resp.Branches, &organization_service.Branch{
			Id:         id.String,
			BranchCode: branch_code.String,
			BranchName: name.String,
			Address:    address.String,
			Phone:      phone.String,
			ShopId:     shop_id.String,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}

	return
}

func (c *branchRepo) Update(ctx context.Context, req *organization_service.UpdateBranch) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"branches"
		SET
			branch_code = :branch_code,
			name = :name,
			address = :address,
			phone = :phone,
			shop_id = :shop_id,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"branch_code": req.GetBranchCode(),
		"name":        req.GetBranchName(),
		"address":     req.GetAddress(),
		"phone":       req.GetPhone(),
		"shop_id":     req.GetShopId(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *branchRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"branches"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *branchRepo) Delete(ctx context.Context, req *organization_service.BranchPrimaryKey) error {
	query := `DELETE FROM "branches" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
