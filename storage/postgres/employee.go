package postgres

import (
	"app/genproto/organization_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type employeeRepo struct {
	db *pgxpool.Pool
}

func NewEmployeeRepo(db *pgxpool.Pool) *employeeRepo {
	return &employeeRepo{
		db: db,
	}
}

func (c *employeeRepo) Create(ctx context.Context, req *organization_service.CreateEmployee) (resp *organization_service.EmployeePrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "employees" (
			id, 
			employee_name,
			employee_phone,
			updated_at
		) VALUES ($1, $2, $3, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.EmployeeName,
		req.EmployeePhone,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.EmployeePrimaryKey{Id: id}, nil
}

func (c *employeeRepo) GetByPKey(ctx context.Context, req *organization_service.EmployeePrimaryKey) (resp *organization_service.Employee, err error) {
	query := `
		SELECT
			id,
			employee_name,
			employee_phone,
			created_at,
			updated_at
		FROM "employees"
		WHERE id = $1
	`

	var (
		id             sql.NullString
		employee_name  sql.NullString
		employee_phone sql.NullString
		created_at     sql.NullString
		updated_at     sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&employee_name,
		&employee_phone,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &organization_service.Employee{
		Id:            id.String,
		EmployeeName:  employee_name.String,
		EmployeePhone: employee_phone.String,
		CreatedAt:     created_at.String,
		UpdatedAt:     updated_at.String,
	}

	return
}

func (c *employeeRepo) GetAll(ctx context.Context, req *organization_service.GetListEmployeeRequest) (resp *organization_service.GetListEmployeeResponse, err error) {
	resp = &organization_service.GetListEmployeeResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			employee_name,
			employee_phone,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "employees"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id             sql.NullString
			employee_name  sql.NullString
			employee_phone sql.NullString
			created_at     sql.NullString
			updated_at     sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&employee_name,
			&employee_phone,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Employees = append(resp.Employees, &organization_service.Employee{
			Id:            id.String,
			EmployeeName:  employee_name.String,
			EmployeePhone: employee_phone.String,
			CreatedAt:     created_at.String,
			UpdatedAt:     updated_at.String,
		})
	}

	return
}

func (c *employeeRepo) Update(ctx context.Context, req *organization_service.UpdateEmployee) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"employees"
		SET
			employee_name = :employee_name,
			employee_phone = :employee_phone,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":            req.GetId(),
		"employee_name":  req.GetEmployeeName(),
		"employee_phone": req.GetEmployeePhone(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *employeeRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"employees"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *employeeRepo) Delete(ctx context.Context, req *organization_service.EmployeePrimaryKey) error {
	query := `DELETE FROM "employees" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
