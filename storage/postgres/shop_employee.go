package postgres

import (
	"app/genproto/organization_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type shopEmployeeRepo struct {
	db *pgxpool.Pool
}

func NewShopEmployeeRepo(db *pgxpool.Pool) *shopEmployeeRepo {
	return &shopEmployeeRepo{
		db: db,
	}
}

func (c *shopEmployeeRepo) Create(ctx context.Context, req *organization_service.CreateShopEmployee) (resp *organization_service.ShopEmployeePrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "shop_employees" (
			id, 
			shop_id,
			employee_id,
			updated_at
		) VALUES ($1, $2, $3, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.ShopId,
		req.EmployeeId,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.ShopEmployeePrimaryKey{Id: id}, nil
}

func (c *shopEmployeeRepo) GetByPKey(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) (resp *organization_service.ShopEmployee, err error) {
	query := `
		SELECT
			id, 
			shop_id,
			employee_id,
			created_at,
			updated_at
		FROM "shop_employees"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		shop_id     sql.NullString
		employee_id sql.NullString
		created_at  sql.NullString
		updated_at  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&shop_id,
		&employee_id,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &organization_service.ShopEmployee{
		Id:         id.String,
		ShopId:     shop_id.String,
		EmployeeId: employee_id.String,
		CreatedAt:  created_at.String,
		UpdatedAt:  updated_at.String,
	}

	return
}

func (c *shopEmployeeRepo) GetAll(ctx context.Context, req *organization_service.GetListShopEmployeeRequest) (resp *organization_service.GetListShopEmployeeResponse, err error) {
	resp = &organization_service.GetListShopEmployeeResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			shop_id,
			employee_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shop_employees"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id          sql.NullString
			shop_id     sql.NullString
			employee_id sql.NullString
			created_at  sql.NullString
			updated_at  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&shop_id,
			&employee_id,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.ShopEmployees = append(resp.ShopEmployees, &organization_service.ShopEmployee{
			Id:         id.String,
			ShopId:     shop_id.String,
			EmployeeId: employee_id.String,
			CreatedAt:  created_at.String,
			UpdatedAt:  updated_at.String,
		})
	}

	return
}

func (c *shopEmployeeRepo) Update(ctx context.Context, req *organization_service.UpdateShopEmployee) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"shop_employees"
		SET
			shop_id = :shop_id,
			employee_id = :employee_id
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":          req.GetId(),
		"shop_id":     req.GetShopId(),
		"employee_id": req.GetEmployeeId(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *shopEmployeeRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"shop_employees"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *shopEmployeeRepo) Delete(ctx context.Context, req *organization_service.ShopEmployeePrimaryKey) error {
	query := `DELETE FROM "shop_employees" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
