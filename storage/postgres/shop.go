package postgres

import (
	"app/genproto/organization_service"
	"app/models"
	"app/pkg/helper"
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type shopRepo struct {
	db *pgxpool.Pool
}

func NewShopRepo(db *pgxpool.Pool) *shopRepo {
	return &shopRepo{
		db: db,
	}
}

func (c *shopRepo) Create(ctx context.Context, req *organization_service.CreateShop) (resp *organization_service.ShopPrimaryKey, err error) {
	var id = uuid.New().String()

	query := `
		INSERT INTO "shops" (
			id, 
			shop_name,
			updated_at
		) VALUES ($1, $2, NOW())
	`

	_, err = c.db.Exec(ctx,
		query,
		id,
		req.ShopName,
	)
	if err != nil {
		return nil, err
	}

	return &organization_service.ShopPrimaryKey{Id: id}, nil
}

func (c *shopRepo) GetByPKey(ctx context.Context, req *organization_service.ShopPrimaryKey) (resp *organization_service.Shop, err error) {
	query := `
		SELECT
			id,
			shop_name,
			created_at,
			updated_at
		FROM "shops"
		WHERE id = $1
	`

	var (
		id         sql.NullString
		shop_name  sql.NullString
		created_at sql.NullString
		updated_at sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&shop_name,
		&created_at,
		&updated_at,
	)
	if err != nil {
		return resp, err
	}

	resp = &organization_service.Shop{
		Id:        id.String,
		ShopName:  shop_name.String,
		CreatedAt: created_at.String,
		UpdatedAt: updated_at.String,
	}

	return
}

func (c *shopRepo) GetAll(ctx context.Context, req *organization_service.GetListShopRequest) (resp *organization_service.GetListShopResponse, err error) {
	resp = &organization_service.GetListShopResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE "
	)

	query = `
		SELECT 
			COUNT(*) OVER(),
			id,
			shop_name,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shops"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}
	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)

	if err != nil {
		return resp, err
	}
	defer rows.Close()

	for rows.Next() {
		var (
			id         sql.NullString
			shop_name  sql.NullString
			created_at sql.NullString
			updated_at sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&shop_name,
			&created_at,
			&updated_at,
		)
		if err != nil {
			return resp, err
		}

		resp.Shops = append(resp.Shops, &organization_service.Shop{
			Id:        id.String,
			ShopName:  shop_name.String,
			CreatedAt: created_at.String,
			UpdatedAt: updated_at.String,
		})
	}

	return
}

func (c *shopRepo) Update(ctx context.Context, req *organization_service.UpdateShop) (rowsAffected int64, err error) {
	var (
		query  string
		params map[string]interface{}
	)

	query = `
		UPDATE
			"shops"
		SET
			shop_name = :shop_name,
			updated_at = now()
		WHERE id = :id
	`

	params = map[string]interface{}{
		"id":        req.GetId(),
		"shop_name": req.GetShopName(),
	}
	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (c *shopRepo) UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {
	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"shops"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err
}

func (c *shopRepo) Delete(ctx context.Context, req *organization_service.ShopPrimaryKey) error {
	query := `DELETE FROM "shops" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)
	if err != nil {
		return err
	}
	return nil
}
