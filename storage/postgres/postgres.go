package postgres

import (
	"app/config"
	"app/storage"
	"context"
	"fmt"
	"log"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type Store struct {
	db            *pgxpool.Pool
	courier       storage.CourierRepoI
	employee      storage.EmployeeRepoI
	shop          storage.ShopRepoI
	shop_employee storage.ShopEmployeeRepoI
	branch        storage.BranchRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(fmt.Sprintf(
		"postgres://%s:%s@%s:%d/%s?sslmode=disable",
		cfg.PostgresUser,
		cfg.PostgresPassword,
		cfg.PostgresHost,
		cfg.PostgresPort,
		cfg.PostgresDatabase,
	))
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections

	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Courier() storage.CourierRepoI {
	if s.courier == nil {
		s.courier = NewCourierRepo(s.db)
	}
	return s.courier
}

func (s *Store) Employee() storage.EmployeeRepoI {
	if s.employee == nil {
		s.employee = NewEmployeeRepo(s.db)
	}
	return s.employee
}

func (s *Store) Shop() storage.ShopRepoI {
	if s.shop == nil {
		s.shop = NewShopRepo(s.db)
	}
	return s.shop
}

func (s *Store) ShopEmployee() storage.ShopEmployeeRepoI {
	if s.shop_employee == nil {
		s.shop_employee = NewShopEmployeeRepo(s.db)
	}
	return s.shop_employee
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branch == nil {
		s.branch = NewbranchRepo(s.db)
	}
	return s.branch
}

func (l *Store) Log(ctx context.Context, level pgx.LogLevel, msg string, data map[string]interface{}) {
	args := make([]interface{}, 0, len(data)+2) // making space for arguments + level + msg
	args = append(args, level, msg)
	for k, v := range data {
		args = append(args, fmt.Sprintf("%s=%v", k, v))
	}
	log.Println(args...)
}
